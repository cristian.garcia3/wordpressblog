<?php
if ( et_theme_builder_overrides_layout( ET_THEME_BUILDER_HEADER_LAYOUT_POST_TYPE ) || et_theme_builder_overrides_layout( ET_THEME_BUILDER_FOOTER_LAYOUT_POST_TYPE ) ) {
    // Skip rendering anything as this partial is being buffered anyway.
    // In addition, avoids get_sidebar() issues since that uses
    // locate_template() with require_once.
    return;
}

/**
 * Fires after the main content, before the footer is output.
 *
 * @since 3.10
 */
do_action( 'et_after_main_content' );

if ( 'on' === et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer id="main-footer">
				<?php get_sidebar( 'footer' ); ?>


		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						?>
					</div>
				</div> <!-- #et-footer-nav -->

			<?php endif; ?>

				<div id="footer-bottom">
					<div class="container clearfix">
				<?php
					if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
						get_template_part( 'includes/social_icons', 'footer' );
					}

					// phpcs:disable WordPress.Security.EscapeOutput.OutputNotEscaped
					echo et_core_fix_unclosed_html_tags( et_core_esc_previously( et_get_footer_credits() ) );
					// phpcs:enable
				?>
					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer(); ?>


<style>
	.facebook_chat{
	    position: fixed;
    bottom: 0;
    right: 30px;
    z-index: 10000;
	}
</style>
<div class="facebook_chat"><?php  echo do_shortcode( '[njwa_button id="947"]' ); ?></div>

<script>
	
	;(function($){
   
		
		setTimeout(function () {
       $("#top-menu a").click(function(e) {
   /* e.preventDefault();
    var aid = $(this).attr("href");
		   
		   if(aid.includes("#")){
			 	 $('html,body').animate({scrollTop: $(aid).offset().top},'slow');
			  }*/
   
});
    }, 2000);
		
})(jQuery);
	
window.addEventListener("DOMContentLoaded", function(){
	
	
    
});
	
	 
</script>


<style>
.container_quote .et_pb_column:first-child{
margin-top:90px

}

	.et_header_style_left #et-top-navigation nav>ul>li>a {
    padding-bottom: 10px !important;
				color:#006A9F 

}

#left-area {
    width: 100% !important;
 }

#main-content .container:before{
	display:none !important
}

.menu-item a:visited{
	color:black !important
}
.menu-item a{
	color:black !important
}


p{
	font-weight:300 !important;
}

a:focus {
   outline:none;
}

h2 span{
	font-weight: 700 !important;
	font-size:30px
}


h1 span{
	font-weight: 700 !important;
	font-size:30px
}


span{
	font-weight:300 !important;
}

.container_columns_landing .et_pb_column .et_pb_module {
	height:530px
}

.slider_container{
	padding-top:0px !important;
}


.last_carousel .slick-list {
    background: #ffffff;
    box-shadow: 0px 35px 30px 0px rgba(0, 0, 0, 0.06);
  
    border-radius: 10px;
}
.container_form_landing .et-last-child{
		width: 50%;
    height: 50%;
    /* overflow: auto; */
    margin: auto;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
}

input{
    position: relative;
    width: 100%;
    margin-bottom: 15px;
    background: #eff1fe;
    border-radius: 4px;
    padding-left: 15px;
    height: 55px;
    line-height: 55px;
    clear: both;
    border: 1px solid #e1e1e1;
	background-color :  #eff1fe !important;
	padding-left:15px !important
}

textarea {
    margin-bottom: 15px;
    border: 1px solid #e1e1e1;
}

textarea {
    background: #eff1fe;
}

textarea {
    padding: 15px 15px;
    height: 150px;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
}

textarea {
    width: 100%; 
}

.center_title_white h5{
	text-align: center;
    text-align: center;
    display: inline;

    margin: 0 auto;
    position: absolute;
    left: 50%;
    transform: translate(-50%, -50%);
    top: 36px;
    background: white;
	padding-left:15px;
	padding-right:15px;
}

.button:hover {
    text-decoration: none;
    color: #ffffff;
    -webkit-transform: translate3d(0, 0px, 0);
    transform: translate3d(0, 0px, 0);
}

.button {
    color: #ffffff;
    padding: 10px 30px;
    border: none;
    position: relative;
    display: inline-block;
    -webkit-border-radius: 0;
    -moz-border-radius: 0;
    border-radius: 0;
    background: #313e5b;
    border-radius: 90px;
    cursor: pointer;
    z-index: 4;
	background-color: #313e5b !important;
    padding-top: 0px !important;
}

h1, h2, h3, h4, h5, h6 {
    font-family: 'Roboto', sans-serif;
    color: #313e5b;
    margin: 0;
    -ms-word-wrap: break-word;
    word-wrap: break-word;
    line-height: 1.5em;
    font-weight: bold;
}

body {
    font-family: 'Roboto', sans-serif;
    font-weight: normal;
    font-style: normal;
    font-size: 1em;
    line-height: 30px;
    color: #525f81;
    overflow-x: hidden !important;
}

.button_action{
	font-size: 16px;
    padding-top: 18px;
    padding-bottom: 18px;
    padding-left: 25px;
    padding-right: 25px;
	color: #fff;
    border: none;
    background-color: #4cadc9;
    background-image: -webkit-linear-gradient(left, #4cadc9 0%, #5472d2 50%,#4cadc9 100%);
    background-image: linear-gradient(to right, #4cadc9 0%, #5472d2 50%,#4cadc9 100%);
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
    background-size: 200% 100%;
	    display: block;
    width: 100%;
    text-align: inherit;
	text-align:center;
}



.button_action:hover{
	    padding-top: 18px !important;
    padding-bottom: 18px !important;
    padding-left: 25px !important;
    padding-right: 25px !important;
    border: none !important;
	    color: #fff;
    border: none;
    background-color: #4cadc9;
    background-image: -webkit-linear-gradient(left, #4cadc9 0%, #5472d2 50%,#4cadc9 100%);
    background-image: linear-gradient(to right, #4cadc9 0%, #5472d2 50%,#4cadc9 100%);
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
    background-size: 200% 100%;
}

.divider_shadow{
	    box-shadow: -10px 10px 10px 1px;
	border : 0px !important;
	height:0
}

.divider_shadow:before{
	    border-top-color: white !important;
	content: '';
    display: block;
    position: absolute;
    left: 0;
    top: -20px;
    right: 0;
    height: 10px;
    border-radius: 100%;
	border-top : 1px ;
	
}

 .vc_sep_holder {
    color: #ebebeb;
}

 .vc_sep_line {
    border-color: #ebebeb;
}

.vc_sep_holder {
    height: 1px;
    position: relative;
    -webkit-box-flex: 1;
    -webkit-flex: 1 1 auto;
    -ms-flex: 1 1 auto;
    flex: 1 1 auto;
    min-width: 10%;
}

.vc_sep_line {
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-orient: horizontal;
    -webkit-box-direction: normal;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-flex-wrap: nowrap;
    -ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -ms-flex-align: center;
    align-items: center;
}


.vc_sep_line {
    border: none;
    position: relative;
    height: 20px;
    top: 0;
    overflow: hidden;
}

.vc_sep_line::after {
    right: -100%;
    box-shadow: 10px 10px 10px 1px;
}

.vc_sep_line::after {
    content: '';
    display: block;
    position: absolute;
    left: 0;
    top: -20px;
    right: 0;
    height: 10px;
    border-radius: 100%;
}

.slick-dots{
	display:none !important;
}

.slide_opacity .carousel-item{
	opacity: 0.5;
	transition: opacity 0.3s, visibility 0.3s;

}

.slide_opacity .carousel-item:hover{
	opacity: 1
}

.video-btn .btn-waves .wave-1 {
    -webkit-animation-delay: 0s;
    animation-delay: 0s;
}
.video-btn .btn-waves .wave-2 {
    -webkit-animation-delay: 0.6s;
    animation-delay: 0.6s;
}


.video-btn .btn-waves .wave-3 {
    -webkit-animation-delay: 1s;
    animation-delay: 1s;
}

.iq-video{
	position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%);
	
	    display: inline-block;
    width: 80px;
    height: 80px;
    line-height: 80px;
    border-radius: 90px;
    background: #ffffff;
    text-align: center;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%);
}

.video-btn{
	position: absolute;
    top: 40%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%,-50%);
}
.main-video .video-btn span {
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%);
	
	    display: inline-block;
    width: 80px;
    height: 80px;
    line-height: 80px;
    border-radius: 90px;
    background: #ffffff;
    text-align: center;
    position: absolute;
    top: 50%;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%, -50%);
}

.video-btn .btn-waves {
    width: 250px;
    height: 250px;
    z-index: 2;
	position: absolute;
    top: 0px !important;
    left: 50%;
    margin-right: -50%;
    transform: translate(-50%,-50%);
}


.video-btn .btn-waves .waves {
    position: absolute;
    width: 250px;
    height: 250px;
    border: 1px solid rgba(255, 255, 255, 0.8);
    opacity: 0;
    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
    border-radius: 320px;
    background-clip: padding-box;
    -webkit-animation: waves 3s ease-in-out infinite;
    animation: waves 3s ease-in-out infinite;
}

@-webkit-keyframes waves {
	0% { -webkit-transform: scale(0.2, 0.2); transform: scale(0.2, 0.2); opacity: 0; -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; }
	50% { opacity: 0.9; -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=90)"; }
	100% { -webkit-transform: scale(0.9, 0.9); transform: scale(0.9, 0.9); opacity: 0; -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; }
}
@keyframes waves {
	0% { -webkit-transform: scale(0.2, 0.2); transform: scale(0.2, 0.2); opacity: 0; -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; }
	50% { opacity: 0.9; -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=90)"; }
	100% { -webkit-transform: scale(0.9, 0.9); transform: scale(0.9, 0.9); opacity: 0; -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)"; }
}

.quote_icon{
	position:absolute
}
.quote_text span{
	    position: relative;
    left: 50px;
}

table{
	border:0px !important;
}
tr{
	border:0px;
}
td{
	border:0px !important;
}

table {
    table-layout: auto;
    border-collapse: collapse;
    width: 100%;
}
table td {
    border: 1px solid #ccc;
}
table .absorbing-column {
    width: 100%;
}

.quote_general{

}

.only_desktop{
	display:block;
}
.only_mobile{
	display:none;
}
	
	.mobile_revolution_slider  {
		max-height: 580px ;
	}
@media only screen and (max-width: 600px) {
	
	
	ul #menu-item-960{
	    text-align: center;
    padding-top: 0 !important;
    margin-bottom: 10px;
		margin-top:10px
	}
	
		
	ul #menu-item-960 a{
	    font-size: 16px;
	}
	
		
	ul #menu-item-792{
	    text-align: center;
    padding-top: 0 !important;
    margin-bottom: 10px;
	}
	
		
	ul #menu-item-792 a{
	    font-size: 16px;
	}
	
	
	.only_mobile{
	display:block;
}
	

.only_desktop{
	display:none;
}
	
	.container_columns_landing .et_pb_column .et_pb_module {
    height: auto;
}
 
.mobile_revolution_slider{
    height: 70vh;
    position: relative;
    top: -0px;
    margin-bottom: 0px !important;
}
	
	.container_quote .et_pb_column:first-child{
margin-top:10px;
 }
		.container_quote {
     min-height: 300px !important;
}
	
	.quote_general {
		display:none
	}
	
	
	.container_image_form{
		display:none
	}
}





#et_search_icon:hover, #top-menu li.current-menu-ancestor>a, #top-menu li.current-menu-item>a, .bottom-nav li.current-menu-item>a, .comment-reply-link, .entry-summary p.price ins, .et-social-icon a:hover, .et_password_protected_form .et_submit_button, .footer-widget h4, .form-submit .et_pb_button, .mobile_menu_bar:after, .mobile_menu_bar:before, .nav-single a, .posted_in a, .woocommerce #content div.product p.price, .woocommerce #content div.product span.price, .woocommerce #content input.button, .woocommerce #content input.button.alt, .woocommerce #content input.button.alt:hover, .woocommerce #content input.button:hover, .woocommerce #respond input#submit, .woocommerce #respond input#submit.alt, .woocommerce #respond input#submit.alt:hover, .woocommerce #respond input#submit:hover, .woocommerce .star-rating span:before, .woocommerce a.button, .woocommerce a.button.alt, .woocommerce a.button.alt:hover, .woocommerce a.button:hover, .woocommerce button.button, .woocommerce button.button.alt, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:hover, .woocommerce div.product p.price, .woocommerce div.product span.price, .woocommerce input.button, .woocommerce input.button.alt, .woocommerce input.button.alt:hover, .woocommerce input.button:hover, .woocommerce-page #content div.product p.price, .woocommerce-page #content div.product span.price, .woocommerce-page #content input.button, .woocommerce-page #content input.button.alt, .woocommerce-page #content input.button.alt:hover, .woocommerce-page #respond input#submit, .woocommerce-page #respond input#submit.alt, .woocommerce-page #respond input#submit.alt:hover, .woocommerce-page #respond input#submit:hover, .woocommerce-page .star-rating span:before, .woocommerce-page a.button, .woocommerce-page a.button.alt, .woocommerce-page a.button.alt:hover, .woocommerce-page a.button:hover, .woocommerce-page button.button, .woocommerce-page button.button.alt, .woocommerce-page button.button.alt.disabled, .woocommerce-page button.button.alt.disabled:hover, .woocommerce-page button.button.alt:hover, .woocommerce-page button.button:hover, .woocommerce-page div.product p.price, .woocommerce-page div.product span.price, .woocommerce-page input.button, .woocommerce-page input.button.alt, .woocommerce-page input.button.alt:hover, .woocommerce-page input.button:hover, .wp-pagenavi a:hover, .wp-pagenavi span.current {
    color: black  !important;
}

.et_header_style_left #et-top-navigation nav>ul>li> .menu-item-792, .et_header_style_split #et-top-navigation nav>ul>li> .menu-item-792 {
    padding-bottom: 0px;
}

ul .menu-item-792{
	  padding: 10px !important;
		padding-bottom: 0px !important;
    background: #007EFF;
    color: white !important;
    border-radius: 50px;
	    font-size: 16px !important;
}
	ul .menu-item-960{
	  padding: 10px !important;
		padding-bottom: 0px !important;
    background: #46C8F4;
    color: white !important;
    border-radius: 50px;
	    font-size: 16px !important;
}
	
	ul #menu-item-960 a{
		color:white !important
	}
		ul #menu-item-960{
	 margin-right:10px
	}
.et_header_style_left #et-top-navigation nav>ul>li>a   {
    padding-bottom:10px !important;
}

 .et_header_style_split #et-top-navigation nav>ul>li>a {
    padding-bottom:10px !important;
}

#top-menu{
	padding-bottom:20px
}


#menu-item-792 a{
	color:white !important;
}


.widget_nav_menu .title{
	color:white !important;
	font-size:18px;
}


.footer-widget .menu li a {
	color:white !important
}


.footer-widget .menu li  {
	list-style:none;
	padding-left:0px !important;
	font-weight:400
 }


.footer-widget .menu  li:before  {
	display:none !important
 }


 
	.leadinModal{
		top:80px !important;
	}
	
	.leadin-content-body {
		    padding-top: 10px;
	}
	#main-header{
		    position: fixed !important;
	}
	
	@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px) 
 { 
	
	 
	.leadinModal{
		top: 80px !important;
	}
	
	}
	
	@media only screen and (max-width: 600px) {
			.leadinModal h4{
	 margin-left: 0;
    margin-top: 10px;
			}
		
			.leadinModal{
		top: 120px !important;
	}
	
	
	}
	
	.menu-item-object-category{
		    padding-left: 10px !important;
    padding-right: 10px  !important;
	}
	
	.sub-menu{
		padding-top: 10px  !important;
    padding-bottom: 10px  !important;
	}
	#moove_gdpr_cookie_info_bar{
		z-index: 900000;
	}
	
	.menu-item a
	{
		color:#006A9F !important
	}
	
	
	.actions .primary {
		font-size: 16px;
     padding-bottom: 18px;
    padding-left: 25px;
    padding-right: 25px;
    color: #fff;
    border: none;
    background-color: #4cadc9;
    background-image: -webkit-linear-gradient(left,#4cadc9 0%,#5472d2 50%,#4cadc9 100%);
    background-image: linear-gradient(to right,#4cadc9 0%,#5472d2 50%,#4cadc9 100%);
    -webkit-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
    background-size: 200% 100%;
    display: block;
    width: 100%;
    text-align: inherit;
    text-align: center;
	}
	#et_search_icon:hover, #top-menu li.current-menu-ancestor>a, #top-menu li.current-menu-item>a, .bottom-nav li.current-menu-item>a, .comment-reply-link, .entry-summary p.price ins, .et-social-icon a:hover, .et_password_protected_form .et_submit_button, .footer-widget h4, .form-submit .et_pb_button, .mobile_menu_bar:after, .mobile_menu_bar:before, .nav-single a, .posted_in a, .woocommerce #content div.product p.price, .woocommerce #content div.product span.price, .woocommerce #content input.button, .woocommerce #content input.button.alt, .woocommerce #content input.button.alt:hover, .woocommerce #content input.button:hover, .woocommerce #respond input#submit, .woocommerce #respond input#submit.alt, .woocommerce #respond input#submit.alt:hover, .woocommerce #respond input#submit:hover, .woocommerce .star-rating span:before, .woocommerce a.button, .woocommerce a.button.alt, .woocommerce a.button.alt:hover, .woocommerce a.button:hover, .woocommerce button.button, .woocommerce button.button.alt, .woocommerce button.button.alt.disabled, .woocommerce button.button.alt.disabled:hover, .woocommerce button.button.alt:hover, .woocommerce div.product p.price, .woocommerce div.product span.price, .woocommerce input.button, .woocommerce input.button.alt, .woocommerce input.button.alt:hover, .woocommerce input.button:hover, .woocommerce-page #content div.product p.price, .woocommerce-page #content div.product span.price, .woocommerce-page #content input.button, .woocommerce-page #content input.button.alt, .woocommerce-page #content input.button.alt:hover, .woocommerce-page #respond input#submit, .woocommerce-page #respond input#submit.alt, .woocommerce-page #respond input#submit.alt:hover, .woocommerce-page #respond input#submit:hover, .woocommerce-page .star-rating span:before, .woocommerce-page a.button, .woocommerce-page a.button.alt, .woocommerce-page a.button.alt:hover, .woocommerce-page a.button:hover, .woocommerce-page button.button, .woocommerce-page button.button.alt, .woocommerce-page button.button.alt.disabled, .woocommerce-page button.button.alt.disabled:hover, .woocommerce-page button.button.alt:hover, .woocommerce-page button.button:hover, .woocommerce-page div.product p.price, .woocommerce-page div.product span.price, .woocommerce-page input.button, .woocommerce-page input.button.alt, .woocommerce-page input.button.alt:hover, .woocommerce-page input.button:hover, .wp-pagenavi a:hover, .wp-pagenavi span.current {
    color: #006A9F !important;
}
	
	
	.menu-item a:visited {
    color: #006A9F !important;
}
 
	.play-icon-general{
		      position: absolute;
    top: 0;
    right: 0;
    color: #006a9f;
    width: 80px;
	}
	
	.container_price_item  {
		min-height: 290px;
		  transition: box-shadow .3s;

		    border: 2px solid #006a9f;
    border-radius: 30px;
    padding: 20px;
		    padding-top: 30px;
    padding-bottom: 30px;
	}
	
	.promoted .button_subscription a{
		   background-color: #4cadc9;
    background-image: -webkit-linear-gradient(left,#4cadc9 0%,#5472d2 50%,#5472d2 100%);
    background-image: linear-gradient(to right,#4cadc9 0%,#5472d2 50%,#5472d2 100%);
	}
	
	.container_price_general span{
		font-weight:bold
	}
	
	
	.price_simbol{
    font-weight: bolder !important;
	}
	
	.container_price_general{
		text-align: center;
    padding-bottom: 10px;
    border-bottom: 1px solid #8080802e;
	}
	
	.container_price_general .price_number{
		    font-size: 27px;
    font-weight: bolder !important;
}
	
	.container_price_general  .price_text_time{
		    font-weight: bolder !important;
    color: #808080ad;
	}
	
	.save_container{
		text-align:center;
	}
	
	.container_price_item:hover{
		  box-shadow: 0 0 11px rgba(33,33,33,.2); 

	}
	
	.save_container span{
		    background: #cecece30;
    border-radius: 10px;
    padding-left: 10px;
    padding-right: 10px;
    padding-top: 5px;
    padding-bottom: 5px;
    font-size: 13px;
    margin: auto;
	}
	
	
	.promoted {
    box-shadow: 0 0 11px rgb(0, 200, 247);
}
	
	.button_subscription a:hover{
		background:#006a9fe3
	}
	.save_container span:hover{
		    background: #cecece20;
	}
	
	.description_container{
		min-height: 50px;
		    font-size: 12px;
			line-height: 16px !important;
			text-align: center;
			color: #808080a6;
	}
	.button_subscription{
		text-align:center
	}

	.button_subscription a{
		background: #006a9f;
    color: white;
    padding-left: 20px;
    padding-right: 20px;
    padding-top: 10px;
    border-radius: 20px;
    padding-bottom: 10px;
	}
</style>

<!-- Start of HubSpot Embed Code -->
<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/5922407.js"></script>
<!-- End of HubSpot Embed Code -->
 

<script>
jQuery("form input").click(function (){
	console.log("aqui estoy mano ");
})
</script>

</body>
</html>
