<?php
namespace ElementorForAmp;

/**
 * Class Plugin
 *
 * Main Plugin class
 * @since 1.2.0
 */
global $amp_elemetor_custom_css;
class Amp_Elementor_Widgets_Loading {
	
	private static $_instance = null;

	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	
	private function include_widgets_files() {	
		require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-accordion.php' );
		require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-tabs.php' );
		require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-counter.php' );
		require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-image-carousel.php' );
		require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-progress.php' );
		require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-toggle.php' );
		require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-video.php' );
		if(class_exists('Brainstorm_Update_UAEL')){
         require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/amp-image-gallery-uea.php' );
        }
		
		require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-image-gallery.php' );
		require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-section.php' );
		if(class_exists("\ElementorPro\Plugin")){

			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-form.php' );
 			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-pricing-table.php' );
 			//Gallery Module
 			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/amp-gallery.php' );
 			//carousel
 			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/carousel/amp-carousel-base.php' );
 			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/carousel/amp-media-carousel.php' );
 			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/carousel/amp-reviews.php' );
 			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/carousel/amp-testimonial-carousel.php' );

 			//Posts
            require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/posts/posts.php' );
            require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/posts/skins/skin-base.php' );
            require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/posts/skins/skin-classic.php' );
            require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/posts/skins/skin-cards.php' );

            //
 			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/amp-share-buttons.php' );
			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/nav-menu.php' );
			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/amp-slides.php' );
			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/animated-headline.php' );
			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/amp-facebook-comments.php' );
			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/amp-post-comments.php' );
			require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/pro/amp-products.php' );
			if (is_plugin_active('raven/raven.php') ){
				require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-jx-nav-menu.php' );
				require_once( AMP_PAGEBUILDER_COMPATIBILITY_PLUGIN_DIR . 'widgets/amp-site-logo.php' );
		    }  
		}
	}
	
	public function register_widgets($widgets_manager) {
		
		// Register Widgets
		if ( (function_exists( 'ampforwp_is_amp_endpoint' ) && ampforwp_is_amp_endpoint()) ||  (function_exists( 'is_wp_amp' ) && is_wp_amp()) || (function_exists( 'is_amp_endpoint' ) && is_amp_endpoint()) ) {

			$this->include_widgets_files();

			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Amp_Accordion() );
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Amp_Tabs() );
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Amp_Counter() );
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Amp_Image_Carousel() );
			if (class_exists("Brainstorm_Update_UAEL")){
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\amp_ue_Image_Gallery() );
            }
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Amp_Progress() );
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Amp_Toggle() );
			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Amp_Video() );
  			\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Amp_Image_Gallery() );
  			\Elementor\Plugin::instance()->elements_manager->register_element_type( new Widgets\AMP_Element_Section() );
 			if(class_exists("\ElementorPro\Plugin")){
 				\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Pro\Amp_Gallery() );
 				\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Amp_Form() );
 				\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Amp_Price_Table() );

               \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Pro\Posts\Posts() );
 				//Carousel
 				\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Pro\Carousel\Media_Carousel() );
 				\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Pro\Carousel\Reviews() );
 				\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Pro\Carousel\Testimonial_Carousel() );
 				\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Pro\Amp_Share_Buttons() );
				 \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Pro\Nav_Menu() );
				 \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Pro\Amp_Slides() );
				\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Pro\Amp_Animated_Headline() );
				\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Pro\AMP_Facebook_Comments() );
				\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Pro\Amp_Post_Comments() );
				\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\Pro\AMP_Products() );
				if (is_plugin_active('raven/raven.php') ){

					\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\AMP_jxNav_mn() );
					\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Widgets\AMP_JxSt_lg() );
	            }
				
 			}
			
		}

	}

	public function __construct() {
		
		// Register widgets		
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'register_widgets' ], 999999 );
	}
}

// Instantiate Plugin Class
Amp_Elementor_Widgets_Loading::instance();
