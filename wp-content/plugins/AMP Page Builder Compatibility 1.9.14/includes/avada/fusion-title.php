<?php
/**
 * Add an element to fusion-builder.
 *
 * @package fusion-builder
 * @since 1.0
 */

if ( fusion_is_element_enabled( 'fusion_title' ) ) {

	if ( ! class_exists( 'AMP_PC_FusionSC_Title' ) ) {
		/**
		 * Shortcode class.
		 *
		 * @since 1.0
		 */
		class AMP_PC_FusionSC_Title extends Fusion_Element {

			/**
			 * Title counter.
			 *
			 * @access protected
			 * @since 1.9
			 * @var integer
			 */
			protected $title_counter = 1;

			/**
			 * An array of the shortcode arguments.
			 *
			 * @access protected
			 * @since 1.0
			 * @var array
			 */
			protected $args;

			/**
			 * Constructor.
			 *
			 * @access public
			 * @since 1.0
			 */
			public $amp_title_styles = array();

			public function __construct() {
				parent::__construct();
				add_filter( 'fusion_attr_title-shortcode', [ $this, 'attr' ] );
				add_filter( 'fusion_attr_title-shortcode-heading', [ $this, 'heading_attr' ] );
				add_filter( 'fusion_attr_animated-text-wrapper', [ $this, 'animated_text_wrapper' ] );
				add_filter( 'fusion_attr_roated-text', [ $this, 'rotated_text_attr' ] );
				add_filter( 'fusion_attr_title-shortcode-sep', [ $this, 'sep_attr' ] );

				add_shortcode( 'fusion_title', [ $this, 'render' ] );
				add_action( 'amp_post_template_css', [ $this, 'amp_pc_title_styles' ] );

			}

			public function amp_pc_title_styles(){
				echo implode('',array_unique($this->amp_title_styles));
			}
			/**
			 * Gets the default values.
			 *
			 * @static
			 * @access public
			 * @since 2.0.0
			 * @return array
			 */
			public static function get_element_defaults() {

				global $fusion_settings;

				return [
					'hide_on_mobile'       => fusion_builder_default_visibility( 'string' ),
					'class'                => '',
					'id'                   => '',
					'title_type'           => 'text',
					'rotation_effect'      => 'bounceIn',
					'display_time'         => '1200',
					'highlight_effect'     => 'circle',
					'loop_animation'       => 'off',
					'highlight_width'      => '9',
					'highlight_top_margin' => '0',
					'before_text'          => '',
					'rotation_text'        => '',
					'highlight_text'       => '',
					'after_text'           => '',
					'content_align'        => 'left',
					'font_size'            => '',
					'animated_font_size'   => '',
					'letter_spacing'       => '',
					'line_height'          => '',
					'margin_bottom'        => $fusion_settings->get( 'title_margin', 'bottom' ),
					'margin_bottom_mobile' => $fusion_settings->get( 'title_margin_mobile', 'bottom' ),
					'margin_top'           => $fusion_settings->get( 'title_margin', 'top' ),
					'margin_top_mobile'    => $fusion_settings->get( 'title_margin_mobile', 'top' ),
					'sep_color'            => $fusion_settings->get( 'title_border_color' ),
					'size'                 => 1,
					'style_tag'            => '',
					'style_type'           => $fusion_settings->get( 'title_style_type' ),
					'text_color'           => '',
					'animated_text_color'  => '',
					'highlight_color'      => '',
				];
			}

			/**
			 * Maps settings to param variables.
			 *
			 * @static
			 * @access public
			 * @since 2.0.0
			 * @return array
			 */
			public static function settings_to_params() {
				return [
					'title_margin[top]'           => 'margin_top',
					'title_margin[bottom]'        => 'margin_bottom',
					'title_margin_mobile[top]'    => 'margin_top_mobile',
					'title_margin_mobile[bottom]' => 'margin_bottom_mobile',
					'title_border_color'          => 'sep_color',
					'title_style_type'            => 'style_type',
				];
			}

			/**
			 * Used to set any other variables for use on front-end editor template.
			 *
			 * @static
			 * @access public
			 * @since 2.0.0
			 * @return array
			 */
			public static function get_element_extras() {
				$fusion_settings = fusion_get_fusion_settings();
				return [
					'content_break_point' => $fusion_settings->get( 'content_break_point' ),
				];
			}

			/**
			 * Maps settings to extra variables.
			 *
			 * @static
			 * @access public
			 * @since 2.0.0
			 * @return array
			 */
			public static function settings_to_extras() {

				return [
					'content_break_point' => 'content_break_point',
				];
			}

			/**
			 * Render the shortcode
			 *
			 * @access public
			 * @since 1.0
			 * @param  array  $args    Shortcode parameters.
			 * @param  string $content Content between shortcode.
			 * @return string          HTML output.
			 */
			public function render( $args, $content = '' ) {

				global $fusion_settings;

				$defaults = FusionBuilder::set_shortcode_defaults( self::get_element_defaults(), $args, 'fusion_title' );
				$defaults = apply_filters( 'fusion_builder_default_args', $defaults, 'fusion_title', $args );
				$content  = apply_filters( 'fusion_shortcode_content', $content, 'fusion_title', $args );

				$defaults['margin_top']           = FusionBuilder::validate_shortcode_attr_value( $defaults['margin_top'], 'px' );
				$defaults['margin_bottom']        = FusionBuilder::validate_shortcode_attr_value( $defaults['margin_bottom'], 'px' );
				$defaults['margin_top_mobile']    = FusionBuilder::validate_shortcode_attr_value( $defaults['margin_top_mobile'], 'px' );
				$defaults['margin_bottom_mobile'] = FusionBuilder::validate_shortcode_attr_value( $defaults['margin_bottom_mobile'], 'px' );

				extract( $defaults );

				$this->args = $defaults;

				if ( 1 === count( explode( ' ', $this->args['style_type'] ) ) ) {
					$style_type .= ' solid';
				}

				if ( ! $this->args['style_type'] || 'default' === $this->args['style_type'] ) {
					$this->args['style_type'] = $style_type = $fusion_settings->get( 'title_style_type' );
				}

				if ( 'text' !== $this->args['title_type'] ) {
					$this->args['style_type'] = $style_type = 'none';
				}
				
				$content           = preg_replace( '!^<p>(.*?)</p>$!i', '$1', trim( $content ) );
				$content = str_replace('</p>', '<br>', $content);
				$content = strip_tags($content,'<br><span>');
				
				$rotation_texts    = [];
				$bottom_highlights = [ 'underline', 'double_underline', 'underline_zigzag', 'underline_zigzag', 'curly' ];

				if ( 'rotating' === $this->args['title_type'] && $rotation_text ) {
					$rotation_texts = explode( '|', trim( $rotation_text ) );
				}

				if ( 'rotating' === $this->args['title_type'] ) {

					$html  = '<div ' . FusionBuilder::attributes( 'title-shortcode' ) . '>';
					$html .= '<h' . $size . ' ' . FusionBuilder::attributes( 'title-shortcode-heading' ) . '>';
					$html .= '<span class="fusion-animated-text-prefix">' . $before_text . '</span> ';

					if ( 0 < count( $rotation_texts ) ) {
						$html .= '<span ' . FusionBuilder::attributes( 'animated-text-wrapper' ) . '>';
						$html .= '<span class="fusion-animated-texts">';

						foreach ( $rotation_texts as $text ) {
							if ( '' !== $text ) {
								$html .= '<span ' . FusionBuilder::attributes( 'roated-text' ) . '>' . $text . '</span>';
							}
						}

						$html .= '</span></span>';
					}

					$html .= ' <span class="fusion-animated-text-postfix">' . $after_text . '</span>';
					$html .= '</h' . $size . '>';
					$html .= '</div>';

				} elseif ( 'highlight' === $this->args['title_type'] ) {

					$html  = '<div ' . FusionBuilder::attributes( 'title-shortcode' ) . '>';
					$html .= '<h' . $size . ' ' . FusionBuilder::attributes( 'title-shortcode-heading' ) . '>';
					$html .= '<span class="fusion-highlighted-text-prefix">' . $before_text . '</span> ';

					if ( $highlight_text ) {
						$html .= '<span class="fusion-highlighted-text-wrapper">';
						$html .= '<span ' . FusionBuilder::attributes( 'animated-text-wrapper' ) . '>' . $highlight_text . '</span>';
						$html .= '</span>';
					}

					$html .= ' <span class="fusion-highlighted-text-postfix">' . $after_text . '</span>';
					$html .= '</h' . $size . '>';
					$html .= '</div>';

				} elseif ( false !== strpos( $style_type, 'underline' ) || false !== strpos( $style_type, 'none' ) ) {

					$html = sprintf(
						'<div %s><h%s %s>%s</h%s></div>',
						FusionBuilder::attributes( 'title-shortcode' ),
						$size,
						FusionBuilder::attributes( 'title-shortcode-heading' ),
						do_shortcode( $content ),
						$size
					);

				} else {
					if ( 'right' === $this->args['content_align'] ) {

						$html = sprintf(
							'<div %s><div %s><div %s></div></div><h%s %s>%s</h%s></div>',
							FusionBuilder::attributes( 'title-shortcode' ),
							FusionBuilder::attributes( 'title-sep-container' ),
							FusionBuilder::attributes( 'title-shortcode-sep' ),
							$size,
							FusionBuilder::attributes( 'title-shortcode-heading' ),
							do_shortcode( $content ),
							$size
						);
					} elseif ( 'center' === $this->args['content_align'] ) {

						$html = sprintf(
							'<div %s><div %s><div %s></div></div><h%s %s>%s</h%s><div %s><div %s></div></div></div>',
							FusionBuilder::attributes( 'title-shortcode' ),
							FusionBuilder::attributes( 'title-sep-container title-sep-container-left' ),
							FusionBuilder::attributes( 'title-shortcode-sep' ),
							$size,
							FusionBuilder::attributes( 'title-shortcode-heading' ),
							do_shortcode( $content ),
							$size,
							FusionBuilder::attributes( 'title-sep-container title-sep-container-right' ),
							FusionBuilder::attributes( 'title-shortcode-sep' )
						);

					} else {

						$html = sprintf(
							'<div %s><h%s %s>%s</h%s><div %s><div %s></div></div></div>',
							FusionBuilder::attributes( 'title-shortcode' ),
							$size,
							FusionBuilder::attributes( 'title-shortcode-heading' ),
							do_shortcode( $content ),
							$size,
							FusionBuilder::attributes( 'title-sep-container' ),
							FusionBuilder::attributes( 'title-shortcode-sep' )
						);
					}
				}

				$style = '<style type="text/css">';

				if ( 'highlight' === $title_type ) {
					if ( $highlight_color ) {
						$style .= '.fusion-title.fusion-title-' . $this->title_counter . ' svg path{stroke:' . fusion_library()->sanitize->color( $highlight_color ) . '!important}';
					}

					if ( $highlight_top_margin && in_array( $highlight_effect, $bottom_highlights, true ) ) {
						$style .= '.fusion-title.fusion-title-' . $this->title_counter . ' svg{margin-top:' . $highlight_top_margin . 'px!important}';
					}

					if ( $highlight_width ) {
						$style .= '.fusion-title.fusion-title-' . $this->title_counter . ' svg path{stroke-width:' . fusion_library()->sanitize->number( $highlight_width ) . '!important}';
					}
				}

				if ( 'rotating' === $title_type && $text_color && ( 'clipIn' === $rotation_effect || 'typeIn' === $rotation_effect ) ) {
					$style .= '.fusion-title.fusion-title-' . $this->title_counter . ' .fusion-animated-texts-wrapper::before{background-color:' . fusion_library()->sanitize->color( $text_color ) . '!important}';
				}

				if ( ! ( '' === $this->args['margin_top_mobile'] && '' === $this->args['margin_bottom_mobile'] ) && ! ( '0px' === $this->args['margin_top_mobile'] && '20px' === $this->args['margin_bottom_mobile'] ) ) {
					$style .= '@media only screen and (max-width:' . $fusion_settings->get( 'content_break_point' ) . 'px) {';
					$style .= '.fusion-title.fusion-title-' . $this->title_counter . '{margin-top:' . $defaults['margin_top_mobile'] . '!important;margin-bottom:' . $defaults['margin_bottom_mobile'] . '!important;}';
					$style .= '}';
				}

				$style .= '</style>';

				$html = $style . $html;
				$heading_attrs = $this->heading_attr();
				$this->amp_title_styles[] = '.fusion-title-'.$this->title_counter.'{'.$heading_attrs['style'].'}';
				
				$this->title_counter++;

				return $html;

			}

			/**
			 * Builds the attributes array.
			 *
			 * @access public
			 * @since 1.0
			 * @return array
			 */
			public function attr() {

				$attr = fusion_builder_visibility_atts(
					$this->args['hide_on_mobile'],
					[
						'class'          => 'fusion-title title fusion-title-' . $this->title_counter,
						'style'          => '',
						'data-highlight' => '',
					]
				);

				if ( false !== strpos( $this->args['style_type'], 'underline' ) ) {
					$styles = explode( ' ', $this->args['style_type'] );

					foreach ( $styles as $style ) {
						$attr['class'] .= ' sep-' . $style;
					}

					if ( $this->args['sep_color'] ) {
						$attr['style'] = 'border-bottom-color:' . $this->args['sep_color'] . ';';
					}
				} elseif ( false !== strpos( $this->args['style_type'], 'none' ) ) {
					$attr['class'] .= ' fusion-sep-none';
				}

				if ( 'center' === $this->args['content_align'] ) {
					$attr['class'] .= ' fusion-title-center';
				}

				if ( $this->args['title_type'] ) {
					$attr['class'] .= ' fusion-title-' . $this->args['title_type'];
				}

				if ( 'text' !== $this->args['title_type'] && $this->args['loop_animation'] ) {
					$attr['class'] .= ' fusion-loop-' . $this->args['loop_animation'];
				}

				if ( 'rotating' === $this->args['title_type'] && $this->args['rotation_effect'] ) {
					$attr['class'] .= ' fusion-title-' . $this->args['rotation_effect'];
				}

				if ( 'highlight' === $this->args['title_type'] && $this->args['highlight_effect'] ) {
					$attr['data-highlight'] .= $this->args['highlight_effect'];
					$attr['class']          .= ' fusion-highlight-' . $this->args['highlight_effect'];

				}

				$title_size = 'one';
				if ( '1' == $this->args['size'] ) { // phpcs:ignore WordPress.PHP.StrictComparisons
					$title_size = 'one';
					$this->amp_title_styles[] = '.fusion-title-size-' . $title_size.'{font-size:34px;}';
				} elseif ( '2' == $this->args['size'] ) { // phpcs:ignore WordPress.PHP.StrictComparisons
					$this->amp_title_styles[] = '.fusion-title-size-' . $title_size.'{font-size:30px;}';
					$title_size = 'two';
				} elseif ( '3' == $this->args['size'] ) { // phpcs:ignore WordPress.PHP.StrictComparisons
					$this->amp_title_styles[] = '.fusion-title-size-' . $title_size.'{font-size:24px;}';
					$title_size = 'three';
				} elseif ( '4' == $this->args['size'] ) { // phpcs:ignore WordPress.PHP.StrictComparisons
					$this->amp_title_styles[] = '.fusion-title-size-' . $title_size.'{font-size:20px;}';
					$title_size = 'four';
				} elseif ( '5' == $this->args['size'] ) { // phpcs:ignore WordPress.PHP.StrictComparisons
					$title_size = 'five';
					$this->amp_title_styles[] = '.fusion-title-size-' . $title_size.'{font-size:16px;}';
				} elseif ( '6' == $this->args['size'] ) { // phpcs:ignore WordPress.PHP.StrictComparisons
					$title_size = 'six';
					$this->amp_title_styles[] = '.fusion-title-size-' . $title_size.'{font-size:10px;}';
				}

				$attr['class'] .= ' fusion-title-size-' . $title_size;

				if ( $this->args['font_size'] ) {
					$attr['style'] .= 'font-size:' . fusion_library()->sanitize->get_value_with_unit( $this->args['font_size'] ) . ';';
				}

				if ( $this->args['margin_top'] ) {
					$attr['style'] .= 'margin-top:' . $this->args['margin_top'] . ';';
				}

				if ( $this->args['margin_bottom'] ) {
					$attr['style'] .= 'margin-bottom:' . $this->args['margin_bottom'] . ';';
				}

				if ( '' === $this->args['margin_top'] && '' === $this->args['margin_bottom'] ) {
					$attr['style'] .= ' margin-top:0px; margin-bottom:0px';
					$attr['class'] .= ' fusion-title-default-margin';
				}

				if ( $this->args['class'] ) {
					$attr['class'] .= ' ' . $this->args['class'];
				}

				if ( $this->args['id'] ) {
					$attr['id'] = $this->args['id'];
				}

				return $attr;

			}

			/**
			 * Builds the heading attributes array.
			 *
			 * @access public
			 * @since 1.0
			 * @return array
			 */
			public function heading_attr() {

				$attr = [
					'class' => 'title-heading-' . $this->args['content_align'],
					'style' => '',
				];

				if ( '' !== $this->args['margin_top'] || '' !== $this->args['margin_bottom'] ) {
					$attr['style'] .= 'margin:0;';
				}

				if ( $this->args['font_size'] ) {
					$attr['style'] .= 'font-size:1em;';
				}

				if ( $this->args['line_height'] ) {
					$attr['style'] .= 'line-height:' . fusion_library()->sanitize->size( $this->args['line_height'] ) . ';';
				}

				if ( $this->args['letter_spacing'] ) {
					$attr['style'] .= 'letter-spacing:' . fusion_library()->sanitize->get_value_with_unit( $this->args['letter_spacing'] ) . ';';
				}

				if ( $this->args['text_color'] ) {
					$attr['style'] .= 'color:' . fusion_library()->sanitize->color( $this->args['text_color'] ) . ';';
				}

				if ( $this->args['style_tag'] ) {
					$attr['style'] .= $this->args['style_tag'];
				}
				
				return $attr;

			}

			/**
			 * Builds the rotated text attributes array.
			 *
			 * @access public
			 * @since 2.1
			 * @return array
			 */
			public function rotated_text_attr() {

				$attr = [
					'data-in-effect'   => $this->args['rotation_effect'],
					'class'            => 'fusion-animated-text',
					'data-in-sequence' => 'true',
					'data-out-reverse' => 'true',
				];

				$attr['data-out-effect'] = str_replace( [ 'In', 'Down' ], [ 'Out', 'Up' ], $this->args['rotation_effect'] );

				return $attr;

			}

			/**
			 * Builds the animated text wrapper attributes array.
			 *
			 * @access public
			 * @since 2.1
			 * @return array
			 */
			public function animated_text_wrapper() {
				$attr = [
					'class' => 'fusion-animated-texts-wrapper',
					'style' => '',
				];

				if ( $this->args['animated_text_color'] ) {
					$attr['style'] .= 'color:' . fusion_library()->sanitize->color( $this->args['animated_text_color'] ) . ';';
				}

				if ( $this->args['animated_font_size'] ) {
					$attr['style'] .= 'font-size:' . fusion_library()->sanitize->get_value_with_unit( $this->args['animated_font_size'] ) . ';';
				}

				if ( 'highlight' === $this->args['title_type'] ) {
					$attr['class'] = 'fusion-highlighted-text';
				}

				if ( 'rotating' === $this->args['title_type'] ) {
					$attr['data-length'] = $this->animation_length();

					if ( $this->args['display_time'] ) {
						$attr['data-minDisplayTime'] = fusion_library()->sanitize->number( $this->args['display_time'] );
					}

					if ( $this->args['after_text'] ) {
						$attr['style'] .= 'text-align: center;';
					}
				}

				return $attr;
			}

			/**
			 * Get animation length based on effect.
			 *
			 * @access public
			 * @since 1.0
			 * @return array
			 */
			public function animation_length() {
				$animation_length = '';

				switch ( $this->args['rotation_effect'] ) {

					case 'flipInX':
					case 'bounceIn':
					case 'zoomIn':
					case 'slideInDown':
					case 'clipIn':
						$animation_length = 'line';
						break;

					case 'lightSpeedIn':
						$animation_length = 'word';
						break;

					case 'rollIn':
					case 'typeIn':
					case 'fadeIn':
						$animation_length = 'char';
						break;
				}

				return $animation_length;
			}

			/**
			 * Builds the separator attributes array.
			 *
			 * @access public
			 * @since 1.0
			 * @return array
			 */
			public function sep_attr() {

				$attr = [
					'class' => 'title-sep',
				];

				$styles = explode( ' ', $this->args['style_type'] );

				foreach ( $styles as $style ) {
					$attr['class'] .= ' sep-' . $style;
				}

				if ( $this->args['sep_color'] ) {
					$attr['style'] = 'border-color:' . $this->args['sep_color'] . ';';
				}

				return $attr;

			}

			/**
			 * Builds the dynamic styling.
			 *
			 * @access public
			 * @since 1.1
			 * @return array
			 */
			public function add_styling() {

				global $wp_version, $content_media_query, $six_fourty_media_query, $three_twenty_six_fourty_media_query, $ipad_portrait_media_query, $fusion_settings, $dynamic_css_helpers;

				$main_elements = apply_filters( 'fusion_builder_element_classes', [ '.fusion-title' ], '.fusion-title' );
				$top_margin    = fusion_library()->sanitize->size( $fusion_settings->get( 'title_margin_mobile', 'top' ) ) . '!important';
				$bottom_margin = fusion_library()->sanitize->size( $fusion_settings->get( 'title_margin_mobile', 'bottom' ) ) . '!important';

				$css[ $content_media_query ][ $dynamic_css_helpers->implode( $main_elements ) ]['margin-top']          = $top_margin;
				$css[ $content_media_query ][ $dynamic_css_helpers->implode( $main_elements ) ]['margin-bottom']       = $bottom_margin;
				$css[ $ipad_portrait_media_query ][ $dynamic_css_helpers->implode( $main_elements ) ]['margin-top']    = $top_margin;
				$css[ $ipad_portrait_media_query ][ $dynamic_css_helpers->implode( $main_elements ) ]['margin-bottom'] = $bottom_margin;

				$elements = array_merge(
					$dynamic_css_helpers->map_selector( $main_elements, ' .title-sep' ),
					$dynamic_css_helpers->map_selector( $main_elements, '.sep-underline' )
				);
				$css['global'][ $dynamic_css_helpers->implode( $elements ) ]['border-color'] = fusion_library()->sanitize->color( $fusion_settings->get( 'title_border_color' ) );

				return $css;

			}

			/**
			 * Adds settings to element options panel.
			 *
			 * @access public
			 * @since 1.1
			 * @return array $sections Title settings.
			 */
			public function add_options() {

				return [
					'title_shortcode_section' => [
						'label'       => esc_html__( 'Title', 'fusion-builder' ),
						'description' => '',
						'id'          => 'title_shortcode_section',
						'type'        => 'accordion',
						'icon'        => 'fusiona-H',
						'fields'      => [
							'title_style_type'    => [
								'label'       => esc_html__( 'Title Separator', 'fusion-builder' ),
								'description' => esc_html__( 'Controls the type of title separator that will display.', 'fusion-builder' ),
								'id'          => 'title_style_type',
								'default'     => 'double solid',
								'type'        => 'select',
								'transport'   => 'postMessage',
								'choices'     => [
									'single solid'     => esc_html__( 'Single Solid', 'fusion-builder' ),
									'single dashed'    => esc_html__( 'Single Dashed', 'fusion-builder' ),
									'single dotted'    => esc_html__( 'Single Dotted', 'fusion-builder' ),
									'double solid'     => esc_html__( 'Double Solid', 'fusion-builder' ),
									'double dashed'    => esc_html__( 'Double Dashed', 'fusion-builder' ),
									'double dotted'    => esc_html__( 'Double Dotted', 'fusion-builder' ),
									'underline solid'  => esc_html__( 'Underline Solid', 'fusion-builder' ),
									'underline dashed' => esc_html__( 'Underline Dashed', 'fusion-builder' ),
									'underline dotted' => esc_html__( 'Underline Dotted', 'fusion-builder' ),
									'none'             => esc_html__( 'None', 'fusion-builder' ),
								],
							],
							'title_border_color'  => [
								'label'       => esc_html__( 'Title Separator Color', 'fusion-builder' ),
								'description' => esc_html__( 'Controls the color of the title separators.', 'fusion-builder' ),
								'id'          => 'title_border_color',
								'default'     => '#e0dede',
								'type'        => 'color-alpha',
								'transport'   => 'postMessage',
								'css_vars'    => [
									[
										'name'     => '--title_border_color',
										'callback' => [ 'sanitize_color' ],
									],
								],
							],
							'title_margin'        => [
								'label'       => esc_html__( 'Title Top/Bottom Margins', 'fusion-builder' ),
								'description' => esc_html__( 'Controls the top/bottom margin of the titles. Leave empty to use corresponding heading margins.', 'fusion-builder' ),
								'id'          => 'title_margin',
								'default'     => [
									'top'    => '0px',
									'bottom' => '31px',
								],
								'transport'   => 'postMessage',
								'type'        => 'spacing',
								'choices'     => [
									'top'    => true,
									'bottom' => true,
								],
							],
							'title_margin_mobile' => [
								'label'       => esc_html__( 'Title Mobile Top/Bottom Margins', 'fusion-builder' ),
								'description' => esc_html__( 'Controls the top/bottom margin of the titles on mobiles. Leave empty together with desktop margins to use corresponding heading margins.', 'fusion-builder' ),
								'id'          => 'title_margin_mobile',
								'transport'   => 'postMessage',
								'default'     => [
									'top'    => '0px',
									'bottom' => '20px',
								],
								'type'        => 'spacing',
								'choices'     => [
									'top'    => true,
									'bottom' => true,
								],
							],
						],
					],
				];
			}

			/**
			 * Sets the necessary scripts.
			 *
			 * @access public
			 * @since 1.1
			 * @return void
			 */
			public function add_scripts() {

				Fusion_Dynamic_JS::enqueue_script(
					'jquery-title-textillate',
					FusionBuilder::$js_folder_url . '/library/jquery.textillate.js',
					FusionBuilder::$js_folder_path . '/library/jquery.textillate.js',
					[ 'jquery' ],
					'2.0',
					true
				);

				Fusion_Dynamic_JS::enqueue_script(
					'fusion-title',
					FusionBuilder::$js_folder_url . '/general/fusion-title.js',
					FusionBuilder::$js_folder_path . '/general/fusion-title.js',
					[ 'jquery' ],
					'1',
					true
				);
			}
		}
	}

	remove_shortcode( 'fusion_title' );
	new AMP_PC_FusionSC_Title();
}
