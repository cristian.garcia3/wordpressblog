<?php
/**
 * Add an element to fusion-builder.
 *
 * @package fusion-builder
 * @since 1.0
 */

if ( fusion_is_element_enabled( 'fusion_tagline_box' ) ) {

	if ( ! class_exists( 'AMP_PC_FusionSC_Tagline' ) ) {
		/**
		 * Shortcode class.
		 *
		 * @since 1.0
		 */
		class AMP_PC_FusionSC_Tagline extends Fusion_Element {

			/**
			 * The tagline box counter.
			 *
			 * @access private
			 * @since 1.0
			 * @var int
			 */
			private $tagline_box_counter = 1;

			/**
			 * An array of the shortcode arguments.
			 *
			 * @access protected
			 * @since 1.0
			 * @var array
			 */
			protected $args;

			/**
			 * Constructor.
			 *
			 * @access public
			 * @since 1.0
			 */
			public $amp_tagline_styles = '';
			public function __construct() {
				parent::__construct();
				add_filter( 'fusion_attr_tagline-shortcode', [ $this, 'attr' ] );
				add_filter( 'fusion_attr_tagline-shortcode-reading-box', [ $this, 'reading_box_attr' ] );
				add_filter( 'fusion_attr_tagline-shortcode-button', [ $this, 'button_attr' ] );

				add_shortcode( 'fusion_tagline_box', [ $this, 'render' ] );
				add_action('amp_post_template_css',[$this,'amp_pc_tagline_styles']);
			}

			/**
			 * Gets the default values.
			 *
			 * @static
			 * @access public
			 * @since 2.0.0
			 * @return array
			 */
			public function amp_pc_tagline_styles(){
				echo $this->amp_tagline_styles;
			}
			public static function get_element_defaults() {
				global $fusion_settings;

				return [
					'hide_on_mobile'       => fusion_builder_default_visibility( 'string' ),
					'class'                => '',
					'id'                   => '',
					'backgroundcolor'      => $fusion_settings->get( 'tagline_bg' ),
					'border'               => '0px',
					'bordercolor'          => $fusion_settings->get( 'tagline_border_color' ),
					'button'               => '',
					'buttoncolor'          => 'default',
					'button_border_radius' => $fusion_settings->get( 'button_border_radius' ),
					'button_size'          => $fusion_settings->get( 'button_size' ),
					'button_type'          => $fusion_settings->get( 'button_type' ),
					'content_alignment'    => 'left',
					'description'          => '',
					'highlightposition'    => 'left',
					'link'                 => '',
					'linktarget'           => '_self',
					'margin_bottom'        => ( '' !== $fusion_settings->get( 'tagline_margin', 'bottom' ) ) ? fusion_library()->sanitize->size( $fusion_settings->get( 'tagline_margin', 'bottom' ) ) : '0px',
					'margin_top'           => ( '' !== $fusion_settings->get( 'tagline_margin', 'top' ) ) ? fusion_library()->sanitize->size( $fusion_settings->get( 'tagline_margin', 'top' ) ) : '0px',
					'modal'                => '',
					'shadow'               => 'no',
					'shadowopacity'        => '0.7',
					'title'                => '',
					'animation_type'       => '',
					'animation_direction'  => 'left',
					'animation_speed'      => '',
					'animation_offset'     => $fusion_settings->get( 'animation_offset' ),
				];
			}

			/**
			 * Maps settings to param variables.
			 *
			 * @static
			 * @access public
			 * @since 2.0.0
			 * @return array
			 */
			public static function settings_to_params() {
				return [
					'tagline_bg'             => 'backgroundcolor',
					'tagline_border_color'   => 'bordercolor',
					'button_border_radius'   => 'button_border_radius',
					'button_size'            => 'button_size',
					'button_type'            => 'button_type',
					'tagline_margin[top]'    => 'margin_top',
					'tagline_margin[bottom]' => 'margin_bottom',
					'animation_offset'       => 'animation_offset',
				];
			}

			/**
			 * Used to set any other variables for use on front-end editor template.
			 *
			 * @static
			 * @access public
			 * @since 2.0.0
			 * @return array
			 */
			public static function get_element_extras() {
				$fusion_settings = fusion_get_fusion_settings();
				return [
					'primary_color' => esc_attr( $fusion_settings->get( 'primary_color' ) ),
				];
			}

			/**
			 * Maps settings to extra variables.
			 *
			 * @static
			 * @access public
			 * @since 2.0.0
			 * @return array
			 */
			public static function settings_to_extras() {

				return [
					'primary_color' => 'primary_color',
				];
			}

			/**
			 * Render the shortcode
			 *
			 * @access public
			 * @since 1.0
			 * @param  array  $args    Shortcode parameters.
			 * @param  string $content Content between shortcode.
			 * @return string          HTML output.
			 */
			public function render( $args, $content = '' ) {

				$defaults = FusionBuilder::set_shortcode_defaults( self::get_element_defaults(), $args, 'fusion_tagline_box' );
				$defaults = apply_filters( 'fusion_builder_default_args', $defaults, 'fusion_tagline_box', $args );
				$content  = apply_filters( 'fusion_shortcode_content', $content, 'fusion_tagline_box', $args );

				$defaults['border'] = FusionBuilder::validate_shortcode_attr_value( $defaults['border'], 'px' );

				if ( $defaults['modal'] ) {
					$defaults['link'] = '#';
				}

				// BC compatibility for button shape.
				if ( isset( $args['button_shape'] ) && ! isset( $args['button_border_radius'] ) ) {
					$args['button_shape'] = strtolower( $args['button_shape'] );

					$button_radius = [
						'square'  => '0px',
						'round'   => '2px',
						'round3d' => '4px',
						'pill'    => '25px',
					];

					if ( '3d' === $defaults['button_type'] && 'round' === $args['button_shape'] ) {
						$args['button_shape'] = 'round3d';
					}

					$defaults['button_border_radius'] = isset( $button_radius[ $args['button_shape'] ] ) ? $button_radius[ $args['button_shape'] ] : $defaults['button_border_radius'];
				}

				$defaults['button_type'] = strtolower( $defaults['button_type'] );

				$defaults['description'] = fusion_decode_if_needed( $defaults['description'] );

				if ( ! empty( htmlspecialchars( fusion_decode_input( $defaults['title'] ) ) ) ) {
					$defaults['title'] = fusion_decode_if_needed( $defaults['title'] );
				}

				extract( $defaults );

				$this->args     = $defaults;
				$desktop_button = $title_tag = $additional_content = '';

				$styles = apply_filters( 'fusion_builder_tagline_box_style', "<style type='text/css'>.reading-box-container-{$this->tagline_box_counter} .element-bottomshadow:before,.reading-box-container-{$this->tagline_box_counter} .element-bottomshadow:after{opacity:{$shadowopacity};}</style>", $defaults, $this->tagline_box_counter );

				if ( isset( $title ) && $title ) {
					$title_tag = '<h2>' . $title . '</h2>';
				}

				$addition_content_class = '';

				if ( isset( $description ) && $description ) {
					if ( isset( $title ) && $title ) {
						$addition_content_class = ' fusion-reading-box-additional';
					}

					$additional_content    .= '<div class="reading-box-description' . $addition_content_class . '">' . $description . '</div>';
					$addition_content_class = '';
				} else {
					if ( isset( $title ) && $title ) {
						$addition_content_class = ' fusion-reading-box-additional';
					}
				}

				if ( $content ) {
					$additional_content .= '<div class="reading-box-additional' . $addition_content_class . '">' . do_shortcode( $content ) . '</div>';
				}

				if ( ( isset( $link ) && $link ) && ( isset( $button ) && $button ) && 'center' !== $this->args['content_alignment'] ) {

					$button_margin_class = '';
					if ( $additional_content ) {
						$button_margin_class = ' fusion-desktop-button-margin';
					}

					$this->args['button_class'] = ' fusion-desktop-button fusion-tagline-button continue' . $button_margin_class;
					$desktop_button             = '<a ' . FusionBuilder::attributes( 'tagline-shortcode-button' ) . '><span>' . $button . '</span></a>';
				}

				if ( $additional_content ) {
					$additional_content .= '<div class="fusion-clearfix"></div>';

					$additional_content = $desktop_button . $title_tag . $additional_content;
				} elseif ( 'center' === $this->args['content_alignment'] ) {
					$additional_content = $title_tag;
				} else {
					$additional_content = '<div class="fusion-reading-box-flex">';
					if ( 'left' === $this->args['content_alignment'] ) {
						$additional_content .= $title_tag . $desktop_button;
					} else {
						$additional_content .= $desktop_button . $title_tag;
					}
					$additional_content .= '</div>';
				}

				if ( ( isset( $link ) && $link ) && ( isset( $button ) && $button ) ) {
					$this->args['button_class'] = ' fusion-mobile-button';
					$additional_content        .= '<a ' . FusionBuilder::attributes( 'tagline-shortcode-button' ) . '><span>' . $button . '</span></a>';
				}

				$html = $styles . '<div ' . FusionBuilder::attributes( 'tagline-shortcode' ) . '><div ' . FusionBuilder::attributes( 'tagline-shortcode-reading-box' ) . '>' . $additional_content . '</div></div>';

				$this->tagline_box_counter++;
				
				return $html;

			}

			/**
			 * Builds the attributes array.
			 *
			 * @access public
			 * @since 1.0
			 * @return array
			 */
			public function attr() {
				$attr = fusion_builder_visibility_atts(
					$this->args['hide_on_mobile'],
					[
						'class' => 'fusion-reading-box-container reading-box-container-' . $this->tagline_box_counter,
					]
				);

				if ( $this->args['animation_type'] ) {
					$animations = FusionBuilder::animations(
						[
							'type'      => $this->args['animation_type'],
							'direction' => $this->args['animation_direction'],
							'speed'     => $this->args['animation_speed'],
							'offset'    => $this->args['animation_offset'],
						]
					);

					$attr = array_merge( $attr, $animations );

					$attr['class'] .= ' ' . $attr['animation_class'];
					unset( $attr['animation_class'] );
				}

				$attr['style'] = '';

				if ( $this->args['margin_top'] || '0' === $this->args['margin_top'] ) {
					$attr['style'] .= 'margin-top:' . fusion_library()->sanitize->get_value_with_unit( $this->args['margin_top'] ) . ';';
				}

				if ( $this->args['margin_bottom'] || '0' === $this->args['margin_bottom'] ) {
					$attr['style'] .= 'margin-bottom:' . fusion_library()->sanitize->get_value_with_unit( $this->args['margin_bottom'] ) . ';';
				}

				if ( $this->args['class'] ) {
					$attr['class'] .= ' ' . $this->args['class'];
				}

				if ( $this->args['id'] ) {
					$attr['id'] = $this->args['id'];
				}

				return $attr;

			}

			/**
			 * Builds the reading-box attributes array.
			 *
			 * @access public
			 * @since 1.0
			 * @return array
			 */
			public function reading_box_attr() {

				global $fusion_settings;

				$attr = [
					'class' => 'reading-box',
				];

				if ( 'right' === $this->args['content_alignment'] ) {
					$attr['class'] .= ' reading-box-right';
				} elseif ( 'center' === $this->args['content_alignment'] ) {
					$attr['class'] .= ' reading-box-center';
				}

				if ( 'yes' === $this->args['shadow'] ) {
					$attr['class'] .= ' element-bottomshadow';
				}

				$attr['style']  = 'background-color:' . $this->args['backgroundcolor'] . ';';
				$attr['style'] .= 'border-width:' . $this->args['border'] . ';';
				$attr['style'] .= 'border-color:' . $this->args['bordercolor'] . ';';
				if ( 'none' !== $this->args['highlightposition'] ) {
					if ( str_replace( 'px', '', $this->args['border'] ) > 3 ) {
						$attr['style'] .= 'border-' . $this->args['highlightposition'] . '-width:' . $this->args['border'] . ';';
					} else {
						$attr['style'] .= 'border-' . $this->args['highlightposition'] . '-width:3px;';
					}
					$attr['style'] .= 'border-' . $this->args['highlightposition'] . '-color:var(--primary_color);';
				}
				$attr['style'] .= 'border-style:solid;';
				$this->amp_tagline_styles = '.reading-box-container-'.$this->tagline_box_counter.' .reading-box{'.$attr['style'].'}';

				return $attr;
			}

			/**
			 * Builds the button attributes array.
			 *
			 * @access public
			 * @since 1.0
			 * @return array
			 */
			public function button_attr() {

				$attr          = [
					'class' => 'button fusion-button button-' . $this->args['buttoncolor'] . ' fusion-button-' . $this->args['button_size'] . ' button-' . $this->args['button_size'] . ' button-' . $this->args['button_type'] . $this->args['button_class'],
					'style' => '',
				];
				$attr['class'] = strtolower( $attr['class'] );

				if ( 'right' === $this->args['content_alignment'] ) {
					$attr['class'] .= ' continue-left';
				} elseif ( 'center' === $this->args['content_alignment'] ) {
					$attr['class'] .= ' continue-center';
				} else {
					$attr['class'] .= ' continue-right';
				}

				if ( 'flat' === $this->args['button_type'] ) {
					$attr['style'] .= '-webkit-box-shadow:none;-moz-box-shadow:none;box-shadow:none;';
				}

				$attr['href']   = $this->args['link'];
				$attr['target'] = $this->args['linktarget'];

				if ( '_blank' === $attr['target'] ) {
					$attr['rel'] = 'noopener noreferrer';
				}

				if ( $this->args['modal'] ) {
					$attr['data-toggle'] = 'modal';
					$attr['data-target'] = '.' . $this->args['modal'];
				}

				if ( $this->args['button_border_radius'] ) {
					$attr['style'] .= 'border-radius:' . ( (int) $this->args['button_border_radius'] ) . 'px;';
				}

				return $attr;

			}

			/**
			 * Builds the dynamic styling.
			 *
			 * @access public
			 * @since 1.1
			 * @return array
			 */
			public function add_styling() {
				global $wp_version, $content_media_query, $six_fourty_media_query, $three_twenty_six_fourty_media_query, $ipad_portrait_media_query, $content_min_media_query, $fusion_settings, $dynamic_css_helpers;

				$main_elements = apply_filters( 'fusion_builder_element_classes', [ '.fusion-reading-box-container' ], '.fusion-reading-box-container' );

				if ( 'yes' === $fusion_settings->get( 'button_span' ) ) {
					$elements = $dynamic_css_helpers->map_selector( $main_elements, ' .fusion-desktop-button' );
					$css['global'][ $dynamic_css_helpers->implode( $elements ) ]['width'] = 'auto';
				}

				$elements = $dynamic_css_helpers->map_selector( $main_elements, ' .reading-box' );
				$css['global'][ $dynamic_css_helpers->implode( $elements ) ]['background-color'] = fusion_library()->sanitize->color( $fusion_settings->get( 'tagline_bg' ) );

				$css[ $content_media_query ]['.fusion-reading-box-container .fusion-reading-box-flex']['display'] = 'block';

				$elements = $dynamic_css_helpers->map_selector( $main_elements, ' .fusion-desktop-button' );
				$css[ $content_media_query ][ $dynamic_css_helpers->implode( $elements ) ]['display']     = 'none';
				$css[ $content_min_media_query ][ $dynamic_css_helpers->implode( $elements ) ]['display'] = 'block';

				$elements = $dynamic_css_helpers->map_selector( $main_elements, ' .fusion-mobile-button' );
				$css[ $content_media_query ][ $dynamic_css_helpers->implode( $elements ) ]['display']       = 'block';
				$css[ $content_min_media_query ][ $dynamic_css_helpers->implode( $elements ) ]['display']   = 'none';
				$css[ $ipad_portrait_media_query ][ $dynamic_css_helpers->implode( $elements ) ]['display'] = 'none';
				$css[ $ipad_portrait_media_query ][ $dynamic_css_helpers->implode( $elements ) ]['float']   = 'none';

				$elements = $dynamic_css_helpers->map_selector( $elements, '.continue-center' );
				$css[ $content_media_query ][ $dynamic_css_helpers->implode( $elements ) ]['display'] = 'block';

				$elements = $dynamic_css_helpers->map_selector( $main_elements, ' .continue-center' );
				$css[ $content_min_media_query ][ $dynamic_css_helpers->implode( $elements ) ]['display'] = 'inline-block';

				$elements = $dynamic_css_helpers->map_selector( $main_elements, ' .reading-box.reading-box-center' );
				$css[ $content_min_media_query ][ $dynamic_css_helpers->implode( $elements ) ]['text-align'] = 'center';

				$elements = $dynamic_css_helpers->map_selector( $main_elements, ' .reading-box.reading-box-right' );
				$css[ $content_min_media_query ][ $dynamic_css_helpers->implode( $elements ) ]['text-align'] = 'right';

				$elements = $dynamic_css_helpers->map_selector( $main_elements, ' .continue' );
				$css[ $ipad_portrait_media_query ][ $dynamic_css_helpers->implode( $elements ) ]['display'] = 'block';

				return $css;

			}

			/**
			 * Adds settings to element options panel.
			 *
			 * @access public
			 * @since 1.1
			 * @return array $sections Tagline settings.
			 */
			public function add_options() {

				return [
					'tagline_box_shortcode_section' => [
						'label'       => esc_html__( 'Tagline Box', 'fusion-builder' ),
						'description' => '',
						'id'          => 'tagline_box_shortcode_section',
						'type'        => 'accordion',
						'icon'        => 'fusiona-list-alt',
						'fields'      => [
							'tagline_bg'           => [
								'label'       => esc_html__( 'Tagline Box Background Color', 'fusion-builder' ),
								'description' => esc_html__( 'Controls the color of the tagline box background.', 'fusion-builder' ),
								'id'          => 'tagline_bg',
								'default'     => '#f6f6f6',
								'type'        => 'color-alpha',
								'transport'   => 'postMessage',
							],
							'tagline_border_color' => [
								'label'       => esc_html__( 'Tagline Box Border Color', 'fusion-builder' ),
								'description' => esc_html__( 'Controls the border color of the tagline box.', 'fusion-builder' ),
								'id'          => 'tagline_border_color',
								'default'     => '#f6f6f6',
								'type'        => 'color-alpha',
								'transport'   => 'postMessage',
							],
							'tagline_margin'       => [
								'label'       => esc_html__( 'Tagline Box Top/Bottom Margins', 'fusion-builder' ),
								'description' => esc_html__( 'Controls the top/bottom margin of the tagline box.', 'fusion-builder' ),
								'id'          => 'tagline_margin',
								'default'     => [
									'top'    => '0px',
									'bottom' => '84px',
								],
								'type'        => 'spacing',
								'transport'   => 'postMessage',
								'choices'     => [
									'top'    => true,
									'bottom' => true,
								],
							],
						],
					],
				];
			}

			/**
			 * Sets the necessary scripts.
			 *
			 * @access public
			 * @since 1.1
			 * @return void
			 */
			public function add_scripts() {

				Fusion_Dynamic_JS::enqueue_script( 'fusion-button' );
			}
		}
	}
	remove_shortcode( 'fusion_tagline_box' );
	new AMP_PC_FusionSC_Tagline();
}
